﻿module serializer.json;

import std.experimental.allocator;
import std.experimental.allocator.mallocator;
import std.meta;

//import container.vector;
public import serializer.common;
import serializer.lexer_utils;
import serializer.lua_json_token;

//  COS==ContainerOrSlice

/**
 * Serializer to save data in json format
 * If serialized data have to be allocated it is not saved/loaded unless it has "malloc" UDA (@("malloc"))
 */
final class JSONSerializer {
	alias SliceElementType = char;
	JSONLexer lex;
	__gshared static JSONSerializerToken tokenSerializer = new JSONSerializerToken();

	int beginObject(Load load, COS)(ref COS con) {
		static if (load == Load.yes) {
			assert(con[0] == '{');
			con ~= con[1 .. $];
		} else {
			con ~= '{';
		}
		return 0; // Just to satisfy interface
	}

	void endObject(Load load, COS)(ref COS con, int begin) {
		static if (load == Load.yes) {
			assert(con[0] == '}');
			con ~= con[1 .. $];
		} else {
			con ~= '}';
		}
	}

	/**
	 * Function loads and saves data depending on compile time variable load
	 * If useMalloc is true pointers, arrays, classes will be saved and loaded using Mallocator
	 * T is the serialized variable
	 * COS is char[] when load==Load.yes 
	 * COS container supplied by user in which data is stored when load==Load.no(save) 
	 */
	void serialize(Load load, bool useMalloc = false, T, COS)(ref T var, ref COS con) {
		try {
			static if (load == Load.yes) {
				lex = JSONLexer(cast(string) con, true, true);
				auto tokens = lex.tokenizeAll();
				//load
				tokenSerializer.serialize!(Load.yes, useMalloc)(var, tokens[]);
				//tokens.clear();
			} else {
				//__gshared static JSONSerializerToken serializer= new JSONSerializerToken();
				TokenDataVector tokens;
				tokenSerializer.serialize!(Load.no, useMalloc)(var, tokens);
				tokensToCharVectorPreatyPrint!(JSONLexer)(tokens[], con);
				//tokens.clear();
			}
		}
		catch (Exception e) {
		}
	}

	//support for rvalues during load
	void serialize(Load load, bool useMalloc = false, T, COS)(ref T var, COS con) {
		static assert(load == Load.yes);
		serialize!(load, useMalloc)(var, con);
	}

	__gshared static JSONSerializer instance = new JSONSerializer();

}

//-----------------------------------------
//--- Tests
//-----------------------------------------

// Helper to avoid GC
private T[n] s(T, size_t n)(auto ref T[n] array) pure nothrow @nogc @safe {
	return array;
}



//-----------------------------------------
//--- Lexer 
//-----------------------------------------

struct JSONLexer {
	enum Token {
		notoken = StandardTokens.notoken,
		white = StandardTokens.white,
		character = StandardTokens.character,
		identifier = StandardTokens.identifier,
		string_ = StandardTokens.string_,
		double_ = StandardTokens.double_,
		long_ = StandardTokens.long_,
	}

	alias characterTokens = AliasSeq!('[', ']', '{', '}', '(', ')', ',', ':');

	string code;
	string slice;
	bool skipUnnecessaryWhiteTokens = true;

	uint line;
	uint column;

	//@disable this();

	this(string code, bool skipWhite, bool skipComments) {
		this.code = code;
		slice = this.code[];
		skipUnnecessaryWhiteTokens = skipWhite;
	}

	void clear() {
		code = null;
		line = column = 0;
		slice = null;
	}

	TokenData checkNextToken() {
		auto sliceCopy = slice;
		auto token = getNextToken();
		slice = sliceCopy;
		return token;
	}

	private TokenData getNextTokenImpl() {
		TokenData token;
		switch (slice[0]) {
			//------- character tokens ------------------------
			foreach (ch; characterTokens) {
		case ch:
			}
			token = slice[0];
			slice = slice[1 .. $];
			return token;

			//--------- white tokens --------------------------
			foreach (ch; whiteTokens) {
		case ch:
			}
			serializeWhiteTokens!(true)(token, slice);
			return token;

			//------- escaped strings -------------------------
		case '"':
			serializeStringToken!(true)(token, slice);
			return token;

			//------- something else -------------------------
		default:
			break;
		}
		if (isIdentifierFirstChar(slice[0])) {
			serializeIdentifier!(true)(token, slice);
		} else if ((slice[0] >= '0' && slice[0] <= '9') || slice[0] == '-') {
			serializeNumberToken!(true)(token, slice);
		} else {
			slice = null;
		}
		return token;

	}

	TokenData getNextToken() {
		TokenData token;
		string sliceCopy = slice[];
		scope (exit) {
			token.line = line;
			token.column = column;
			updateLineAndCol(line, column, sliceCopy, slice);
		}
		while (slice.length > 0) {
			token = getNextTokenImpl();
			if (skipUnnecessaryWhiteTokens && token.type == Token.white) {
				token = TokenData.init;
				continue;
			}
			break;
		}
		return token;
	}

	static void toChars(Vec)(TokenData token, ref Vec vec) {
		final switch (cast(Token) token.type) {
		case Token.long_:
		case Token.double_:
			serializeNumberToken!(false)(token, vec);
			break;
		case Token.character:
			vec ~= token.ch;
			break;
		case Token.white:
		case Token.identifier:
			vec ~= cast(char[]) token.str;
			break;
		case Token.string_:
			vec ~= '"';
			vec ~= cast(char[]) token.getEscapedString();
			vec ~= '"';
			break;

		case Token.notoken:
			assert(0);
		}

	}

}

unittest {
	string code = `{ [ ala: "asdasd", ccc:123.3f]}"`;
	JSONLexer json = JSONLexer(code, true, true);
	TokenData token;
	token.type = StandardTokens.identifier;
	token.str = "asd";
}



alias JSONSerializerToken = JSON_Lua_SerializerToken!(true);

//-----------------------------------------
//--- Tests
//-----------------------------------------
