module mframe.crypto;

import core.stdc.stdio;
import std.string : toStringz;
import std.stdio;

import tomcrypt.cipher;
import tomcrypt.tomcrypt;
import tomcrypt.misc;

import serializer.json;


struct Cipher
{
    enum keySize = 16;
    ubyte[keySize] key;
    ubyte[keySize] IV;
    symmetric_CTR ctr;

    void initialize(ubyte[keySize] key, ubyte[keySize] IV)
    {
        this.key = key;
        this.IV = IV;

        if (register_cipher(&twofish_desc) == -1)
        {
            assert(0);
        }

        int err;
        if ((err = ctr_start(find_cipher("twofish"), IV.ptr, key.ptr, keySize,
                0, CTR_COUNTER_LITTLE_ENDIAN, &ctr)) != CRYPT_OK)
        {
            assert(0);
        }
    }

    ~this()
    {
        int err;
        if ((err = ctr_done(&ctr)) != CRYPT_OK)
        {
            assert(0);
        }
        key = 0;
        ctr = symmetric_CTR.init;
    }

    void encrypt(ubyte[] arr)
    {
        int err;
        if ((err = ctr_setiv(IV.ptr, keySize, &ctr)) != CRYPT_OK)
        {
            printf("ctr_done error: %s\n", error_to_string(err));
            assert(0);
        }
        if ((err = ctr_encrypt(arr.ptr, arr.ptr, arr.length, &ctr)) != CRYPT_OK)
        {
            assert(0);
        }
    }

    void decrypt(ubyte[] arr)
    {
        int err;
        if ((err = ctr_setiv(IV.ptr, keySize, &ctr)) != CRYPT_OK)
        {
            printf("ctr_done error: %s\n", error_to_string(err));
            assert(0);
        }
        if ((err = ctr_decrypt(arr.ptr, arr.ptr, arr.length, &ctr)) != CRYPT_OK)
        {
            assert(0);
        }
    }

    string toEncryptedBase64(const(ubyte[]) data)
    {
        ubyte[] dataCopy = data.dup;
        encrypt(dataCopy);
        return Base64.encode(dataCopy);
    }

    string toDecryptedString(const(char[]) base64)
    {
        ubyte[] data = Base64.decode(base64);
        encrypt(data);
        char[] dataChar = cast(char[])data;
        return dataChar.idup;
    }

    string typeToEncryptedBase64JSON(T)(ref T data)
    {
        char[] container;
        JSONSerializer.instance.serialize!(Load.no, true)(data, container);
        return toEncryptedBase64(cast(ubyte[])container);
    }

    T encryptedBase64JSONToType(T)(const(char[]) base64)
    {
        T data;
        string dataStr = toDecryptedString(base64);
        JSONSerializer.instance.serialize!(Load.yes, true)(data, dataStr);
        return data;
    }

}

struct Base64
{
    static ubyte[1024] buffer;

    static string encode(ubyte[] arr)
    {
        ulong len = buffer.length;
        int err = base64_encode(arr.ptr, arr.length, buffer.ptr, &len);
        assert(err == CRYPT_OK);
        return (cast(char[])buffer[0..len]).idup;
    }

    static ubyte[] decode(const(char[]) arr)
    {
        ulong len = buffer.length;
        int err = base64_decode(cast(ubyte*)arr.ptr, arr.length, buffer.ptr, &len);
        assert(err == CRYPT_OK);
        return (buffer[0..len]).dup;
    }

}

