module mframe.multipart;

import std.algorithm : countUntil, splitter, strip;


enum ContentDispositionType
{
    Inline,
    Attachment,
    FormData,
}

struct ContentDisposition
{
    ContentDispositionType type;
    char[] name;
    char[] filename;
}

struct ContentType
{
    char[] type;
    char[] charset;
}

struct MultipartPartHeaderInfo
{
    ContentDisposition contentDisposition;
    ContentType contentType;
}

struct MultipartPart
{
    MultipartPartHeaderInfo header;
    ubyte[] value;
}

/// Transformation: name="fileToUpload" => ["name", "fileToUpload"]
private char[][2] getKeyVal(char[] data)
{
    char[][2] keyVal;
    size_t charPos = data.countUntil('=');
    if (charPos == -1)
    {
        keyVal[0] = data.strip(' ');
        return keyVal;
    }
    keyVal[0] = data[0 .. charPos].strip(' ');
    keyVal[1] = data[charPos + 1 .. $].strip('"');
    return keyVal;
}

MultipartPartHeaderInfo parseMultipartHeader(char[] header, const(char)[] lineSeparator)
{
    MultipartPartHeaderInfo info;

    auto lines = header.splitter(lineSeparator);
    foreach (line; lines)
    {
        size_t colonPos = line.countUntil(':');
        char[] headerType = line[0 .. colonPos];
        char[] headerData = line[colonPos + 1 .. $].strip(' ');
        auto dataParts = headerData.splitter(';');

        if (headerType == "Content-Type")
        {

            assert(dataParts.empty == false);
            info.contentType.type = dataParts.front();
            dataParts.popFront();
            foreach (part; dataParts)
            {
                auto keyVal = getKeyVal(part);
                if (keyVal[0] == "charset")
                {
                    info.contentType.charset = keyVal[1];
                }
            }
        }
        else if (headerType == "Content-Disposition")
        {

            assert(dataParts.empty == false);
            char[] type = dataParts.front();
            dataParts.popFront();
            switch (type)
            {
            case "inline":
                info.contentDisposition.type = ContentDispositionType.Inline;
                break;
            case "attachment":
                info.contentDisposition.type = ContentDispositionType.Attachment;
                break;
            case "form-data":
                info.contentDisposition.type = ContentDispositionType.FormData;
                break;
            default:
                break;
            }
            foreach (part; dataParts)
            {
                auto keyVal = getKeyVal(part);
                if (keyVal[0] == "name")
                {
                    info.contentDisposition.name = keyVal[1];
                }
                else if (keyVal[0] == "filename")
                {
                    info.contentDisposition.filename = keyVal[1];
                }
            }
        }

    }
    return info;
}

unittest
{
    char[] headerStr = `Content-Disposition: form-data; name="file_form_name"; filename="file.txt"
Content-Type: application/octet-stream; charset=utf-8`.dup;
    MultipartPartHeaderInfo header = parseMultipartHeader(headerStr, "\n");
    assert(header.contentDisposition.type == ContentDispositionType.FormData);
    assert(header.contentDisposition.name == "file_form_name");
    assert(header.contentDisposition.filename == "file.txt");
    assert(header.contentType.type == "application/octet-stream");
    assert(header.contentType.charset == "utf-8");
}

MultipartPart[] parseMultipart(ubyte[] data, string contentType, const(char)[] lineSeparator)
{
    const(char)[] twoSeparators = lineSeparator ~ lineSeparator;
    MultipartPart[] multiparts;
    string boundary = contentType[contentType.countUntil('=')+1..$];
    boundary = "--" ~ boundary;
    auto multipart_key_val = data.splitter(boundary);
    foreach (ref key_val; multipart_key_val)
    {
        auto start = countUntil(key_val, twoSeparators);
        if(start <= 0)
        {
            continue;
        }
        char[] header = cast(char[])key_val[lineSeparator.length .. start];
        ubyte[] val = key_val[start + twoSeparators.length .. $-twoSeparators.length/2];
        
        MultipartPart part;
        part.header = parseMultipartHeader(header, lineSeparator);
        part.value = val;
        multiparts ~= part;

    }
    return multiparts;
}

unittest
{
    char[] multipartStr = `-----------------------------12091938642678857883034252035
Content-Disposition: form-data; name="fileToUpload"; filename="aaaa"
Content-Type: application/octet-stream

AAABBB
-----------------------------12091938642678857883034252035
Content-Disposition: form-data; name="fileToUpload22"; filename="aaaa"
Content-Type: application/octet-stream

AAABBB
-----------------------------12091938642678857883034252035
Content-Disposition: form-data; name="submit"

Upload Image
-----------------------------12091938642678857883034252035--`.dup;
    ubyte[] multipartData = cast(ubyte[])multipartStr;

    MultipartPart[] multipart = parseMultipart(multipartData, "multipart/form-data; boundary=---------------------------12091938642678857883034252035", "\n");
    assert(multipart.length == 3);
    assert(multipart[0].value == cast(ubyte[])"AAABBB");
    assert(multipart[1].value == cast(ubyte[])"AAABBB");
    assert(multipart[2].value == cast(ubyte[])"Upload Image");
}
