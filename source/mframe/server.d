module mframe.server;

import core.stdc.string;
import std.conv : to;
import std.algorithm;
import std.stdio;
import std.string : toStringz, indexOf;

import fcgi.fcgi;

import mframe.multipart;
import mframe.request;
import mframe.router;
import mframe.url;
import mmutils.log;

ulong gHandledRequests = 0;

// TODO: improve implementation
string[string] parseCookie(string str) pure
{
    string[string] cookies;
    auto cookies_key_val = str.splitter(';');
    foreach (ref key_val; cookies_key_val)
    {
        auto start = countUntil(key_val, '=');
        string key = key_val[0 .. start];
        string val = key_val[start + 1 .. $];
        cookies[key] = val;

    }
    return cookies;
}

unittest
{
    auto ret = parseCookie("name=Koks;age=23");
    assert(ret["name"] == "Koks");
    assert(ret["age"] == "23");
}

// TODO: improve implementation
string[string] parseGet(const(char)[] str)
{
    string[string] cookies;
    auto cookies_key_val = str.splitter('&');
    foreach (ref key_val; cookies_key_val)
    {
        auto start = countUntil(key_val, '=');
        if(start <= 0)
        {
            continue;
        }
        string key = key_val[0 .. start].idup;
        string val = key_val[start + 1 .. $].idup;
        if(val.length==0)
        {
            cookies[key] = "";
            continue;
        }
        cookies[key] = url_decode(val);
    }
    return cookies;
}


unittest
{
    auto ret = parseGet("name=Koks&age=23");
    assert(ret["name"] == "Koks");
    assert(ret["age"] == "23");
}




struct Server
{
    FCGX_Request request;
    Router router;
    
    

    void initialize(Router router)
    {
        this.router = router;

        // writeln("Registred controllers:");
        // foreach(key; router.controlers.byKey)
        // {
        //     writeln(key);
        // }
    }

    const(char)[] last_name;
   
   

    void start()
    {
        int fcgiInitCode = FCGX_Init();
        assert(fcgiInitCode == 0, "Failed to initialzie FCGX_Init()");
        int socketFileDescriptor = FCGX_OpenSocket(":9001", 500);
        // writeln("Socket file descriptor: ", socketFileDescriptor);
        int fcgiInitReqCode = FCGX_InitRequest(&request, socketFileDescriptor, 0);
        assert(fcgiInitReqCode == 0, "Failed to initialzie FCGX_InitRequest()");

        while (FCGX_Accept_r(&request) == 0)
        {

            gHandledRequests++;
            scope (exit)
            {
                FCGX_Finish_r(&request);
            }
            char* getCStr = FCGX_GetParam("QUERY_STRING", request.envp);
            char* pageFullCStr = FCGX_GetParam("REQUEST_URI", request.envp);
            char* cookieCStr = FCGX_GetParam("HTTP_COOKIE", request.envp);
            char* contentTypeCStr = FCGX_GetParam("HTTP_CONTENT_TYPE", request.envp);
            string get = getCStr[0 .. strlen(getCStr)].idup;
            string pageFull = pageFullCStr[0 .. strlen(pageFullCStr)].idup;
            string contentType = contentTypeCStr ? contentTypeCStr[0 .. strlen(contentTypeCStr)].idup : null;
            string page = pageFull[0 .. pageFull.length - get.length];
            string cookie = "";
            if (cookieCStr)
            {
                cookie = cookieCStr[0 .. strlen(cookieCStr)].idup;

            }
            if (page[$ - 1] == '?')
            {
                page = page[0 .. $ - 1];
            }

            auto perfCheck = log_perf(LL(LogType.Info, LogPriority.VeryImportant), "End Request\n", "Start Request: %s", pageFull.toStringz);

            ubyte[] post;
            //writefln("PAGE: '%s'", page);
            //writefln("GET: '%s'", get);
            //writeln("POST: ");
            //writeln("cookie: ", cookie);
            while (1)
            {
                int ret = FCGX_GetChar(request.in_);
                if (ret == -1)
                {
                    break;
                }
                post ~= cast(ubyte) ret;
            }
                log(LL(), "ContentType(%.*s)", cast(int)contentType.length, contentType.ptr);

            ResponseData responseData;
            RequestData data;

            data.page = page;
            if(contentType.countUntil("multipart") == -1)
            {
                data.post = parseGet(cast(char[])post);
            }else
            {
                data.multipart = parseMultipart(post, contentType, "\r\n");
            }

            data.get = parseGet(get);
            data.cookie = parseCookie(cookie);
            router.requestDataInitFunc(data);

            string output;
            ControlerData controllerData = router.getControler(data.page);
            if(controllerData.requiredTrustLevel > data.trustLevel)
            {
                responseData.statusCode = StatusCode.unauthorized;
                writefln("User not authorized, requiredTrustLevel(%d), userTrustLevel(%d)", controllerData.requiredTrustLevel, data.trustLevel);
            }
            else
            {
                try
                {
                    output = controllerData.func(data, responseData);
                }
                catch (Exception e)
                {
                    writeln(e);
                }
            }
            if (responseData.statusCode != StatusCode.ok)
            {
                //FCGX_SetExitStatus(responseData.statusCode, request.out_);

                FCGX_FPrintF(request.out_, "HEAD / HTTP/1.0\r\n");
                FCGX_FPrintF(request.out_, "Status: %d\r\n", responseData.statusCode);
                FCGX_FPrintF(request.out_, "\r\n\r\n");
                continue;
            }

            FCGX_FPrintF(request.out_, "Status: %d\r\n", responseData.statusCode);
            FCGX_FPrintF(request.out_, "Content-type: application/json\r\n");

            foreach (keyVal; responseData.setCookie.byKeyValue)
            {
                Cookie c = keyVal.value;
                string secure = c.secure ? " ; Secure" : "";
                string httpOnly = c.httpOnly ? " ; HttpOnly" : "";
                FCGX_FPrintF(request.out_, "Set-Cookie: %.*s=%.*s; Path=/ ; Max-Age=%d%s%s\r\n",
                        cast(int)keyVal.key.length, keyVal.key.ptr, cast(int)c.value.length,c.value.ptr, c.expireDate, secure.toStringz, httpOnly.toStringz
                        );
            }

            if (responseData.redirectPath)
            {
                FCGX_FPrintF(request.out_, "Location: %s\r\n", responseData.redirectPath.ptr);
            }

            FCGX_FPrintF(request.out_, "\r\n");
            FCGX_PutStr(output.ptr, cast(int) output.length, request.out_);

        }
    }
}
