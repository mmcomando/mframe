module mframe.url;

import core.stdc.ctype;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

/// Converts a hex character to its integer value
char from_hex(char ch)
{
    return cast(char)(isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10);
}

/// Converts an integer value to its hex characte
char to_hex(char code)
{
    static string hex = "0123456789abcdef";
    return hex[code & 15];
}

/// Returns a url-encoded version of str
/// IMPORTANT: be sure to free() the returned string after use
char* url_encode(char* str)
{
    char* pstr = str;
    char* buf = cast(char*)malloc(strlen(str) * 3 + 1);
    char* pbuf = buf;
    while (*pstr)
    {
        if (isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~')
            *pbuf++ = *pstr;
        else if (*pstr == ' ')
            *pbuf++ = '+';
        else
            *pbuf++ = '%', *pbuf++ = to_hex(*pstr >> 4), *pbuf++ = to_hex(*pstr & 15);
        pstr++;
    }
    *pbuf = '\0';
    return buf;
}

/// Returns a url-decoded version of str
/// IMPORTANT: be sure to free() the returned string after use
char* url_decode(char* str, size_t size)
{
    char* pstr = str;
    char* buf = cast(char*)calloc(strlen(str) + 1, 1);
    char* pbuf = buf;
    while (*pstr)
    {
        if(cast(int)(pstr-str>=size))
        {
            return buf;
        }
        if (*pstr == '%')
        {
            if (pstr[1] && pstr[2])
            {
                *pbuf++ = cast(char)(from_hex(pstr[1]) << 4 | from_hex(pstr[2]));
                pstr += 2;
            }
        }
        else if (*pstr == '+')
        {
            *pbuf++ = ' ';
        }
        else
        {
            *pbuf++ = *pstr;
        }
        pstr++;
    }
    *pbuf = '\0';
    return buf;
}

string url_encode(string str)
{
    char * cStr = url_encode(cast(char*)str.ptr);
    string modified = cStr[0 .. strlen(cStr)].idup;
    free(cStr);
    return modified;
}

string url_decode(string str)
{
    char * cStr = url_decode(cast(char*)str.ptr, str.length);
    string modified = cStr[0 .. strlen(cStr)].idup;
    free(cStr);
    return modified;
}
