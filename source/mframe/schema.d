module mframe.schema;

import std.traits : isIntegral, isNumeric, getUDAs;
import std.uni : toLower;

/// Type for UDA PostgreSQL table name change
/// Ex. change 'User' struct to 'person' table in postgresql as 'user' is restricted table name
struct PostgreSQLTableName
{
    string name;/// Table name
}

/// Metadata about data stored in database
/// Used to create schema in database engines
/// Based on json schema format, https://json-schema.org/
struct Schema
{
    /// Schema name
    string title;
    /// "object"
    string type;
    /// Description of column/table
    string description;
    /// Table name in PostgresSQL
    string postgresTableName;
    /// Columns present in schema
    Property[string] properties;
}

/// Describes columns
struct Property
{
    /// Column name
    string title;
    /// Column type
    string type;
    /// Description of column
    string description;
    /// Display order, lowest number should be displayed first
    int displayOrder;
}

/// Creates schema from fiven type
Schema createSchemaFromType(T)(string schemaName)
{
    Schema schema;
    schema.title = T.stringof.toLower;
    schema.postgresTableName = schemaName;
    schema.type = "string";
    static if (is(T == struct))
    {
        schema.type = "object";
        T obj;
        foreach (i, elem; obj.tupleof)
        {
            alias Type = typeof(elem);
            string name = __traits(identifier, obj.tupleof[i]);
            Property property = createProperty!Type(name);
            property.displayOrder = i;
            schema.properties[name] = property;
        }

    }
    else
    {
        pragma(msg, T);
        static assert(0);
    }

    return schema;

}

Property createProperty(T)(string name)
{
    Property property;
    property.title = name;
    static if (is(T == string))
    {
        property.type = "string";

    }
    else static if (isIntegral!T)
    {
        property.type = "integer";

    }
    else static if (isNumeric!T)
    {
        property.type = "numeric";

    }
    else
    {
        pragma(msg, T);
        static assert(0);
    }

    return property;
}

Schema creteSchema(T)()
{
    alias tableNameUda = getUDAs!(T, PostgreSQLTableName);
    static if (tableNameUda.length == 1)
    {
        string tableName = tableNameUda[0].name;
    }
    else
    {
        string tableName = T.stringof.toLower;
    }
    return createSchemaFromType!T(tableName);
}

Schema[] creteSchemas(Schemas...)()
{
    Schema[] schemas;
    foreach (i, Type; Schemas)
    {
        schemas ~= creteSchema!Type();
    }
    return schemas;
}

// string someSchema = `{
//   "$id": "https://example.com/person.schema.json",
//   "$schema": "http://json-schema.org/draft-07/schema#",
//   "title": "person",
//   "type": "object",
//   "properties": {
//     "firstName": {
//       "type": "string",
//       "description": "The person's first name."
//     },
//     "name": {
//       "type": "string",
//       "description": "The person's first name."
//     },
//     "password": {
//       "type": "string",
//       "description": "The person's first name."
//     },
//     "lastName": {
//       "type": "string",
//       "description": "The person's last name."
//     },
//     "age": {
//       "description": "Age in years which must be equal to or greater than zero.",
//       "type": "integer",
//       "minimum": 0
//     }
//   }
// }
// `;
