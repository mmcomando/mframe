module mframe.controlers.postgresql;

import std.stdio;

import mframe.controlers.schema;
import mframe.db.postgresql;
import mframe.request;
import mframe.router;
import mframe.schema;
import mframe.trust;

struct PostgreSQLAdminController
{
    struct QueryResult
    {
        string[] columnNames;
        string[][] rows;
    }

    SchemaGroupApis[string] gPostgresSchemasApis;

    void initialize()
    {
        string[] databases = getDatabases();
        foreach (string databaseName; databases)
        {
            Schema[] schemas;

            string[] tables = getTables(databaseName);
            foreach (string tableName; tables)
            {
                Schema schema;
                schema.type = "object";
                schema.title = tableName;
                schema.postgresTableName = tableName;

                string[] columns = getColumns(databaseName, tableName);
                foreach (i, string columnName; columns)
                {
                    Property property;

                    property.title = columnName;
                    property.type = "string";
                    property.displayOrder = cast(int)i;

                    schema.properties[columnName] = property;
                }

                schemas ~= schema;
            }

            SchemaGroupApis api;
            api.initialize(getConnection(databaseName), schemas);
            gPostgresSchemasApis[databaseName] = api;
        }
    }

    void register(ref Router router)
    {
        router.addJSON("/api/postgresql/execute_query", &executeQuery, TrustLevel.ApplicationCreator);
        router.addJSON("/api/postgresql/databases", &getDatabasesApi, TrustLevel.ApplicationCreator);

        foreach (ref key_val; gPostgresSchemasApis.byKeyValue)
        {
            string dbPath = "/api/postgresql/db/" ~ key_val.key ~ "/";
            key_val.value.register(router, dbPath);
        }
    }

    private:

    PostgresConnection getConnection(string dbName)
    {
        PostgresConnection connection;
        connection.initiaizeEnv();
        connection.reconnectionTryCount = 1;
        connection.dbName = dbName;
        connection.createDatabaseOnStartup = false;
        return connection;
    }



    QueryResult executeSelectQueryInDB(Args...)(string dbName, string query, Args args)
    {
        PostgresConnection conn = getConnection(dbName);
        scope (exit)
            conn.disconnect();
        auto result = conn.query(query, args);
        QueryResult res;
        res.rows = conn.getResultAsStrings(result);
        res.columnNames = conn.getResultColumnNames(result);
        return res;
    }

    string executeQuery(const RequestData requestData, out ResponseData responseData)
    {
        string output;
        try
        {
            string dbName = requestData.get.get("dbName", "");
            string query = requestData.get.get("query", "");
            writeln(dbName, query);

            PostgresConnection conn = getConnection(dbName);
            scope (exit)
                conn.disconnect();

            auto result = conn.query(query);
            output = conn.resultPrintToString(result);
        }
        catch (Exception e)
        {
            output = e.toString();
        }
        responseData.statusCode = StatusCode.ok;
        return output;
    }

    string[] getDatabases()
    {
        QueryResult queryResult = executeSelectQueryInDB("postgres", "SELECT datname FROM pg_database");

        string[] databases;
        foreach (string[] row; queryResult.rows)
        {
            string databaseName = row[0];
            if (
                databaseName == "template0" || 
                databaseName == "template1" || 
                databaseName == "postgres"
                )
            {
                continue; // Fitler out postgresql databases without useful data
            }
            databases ~= row[0];
        }
        return databases;
    }

    string[] getTables(string database)
    {
        string query = "SELECT tablename FROM pg_tables WHERE schemaname='public'";
        QueryResult queryResult = executeSelectQueryInDB(database, query);
        string[] tables;
        foreach (string[] row; queryResult.rows)
        {
            tables ~= row[0];
        }
        return tables;
    }

    string[] getColumns(string database, string table)
    {
        // For SELECT * columns are:
        // table_catalog|table_schema|table_name|column_name|ordinal_position|column_default|is_nullable|data_type        |character_maximum_length|character_octet_length|numeric_precision|numeric_precision_radix|numeric_scale|datetime_precision|interval_type|interval_precision|character_set_catalog|character_set_schema|character_set_name|collation_catalog|collation_schema|collation_name|domain_catalog|domain_schema|domain_name|udt_catalog|udt_schema|udt_name|scope_catalog|scope_schema|scope_name|maximum_cardinality|dtd_identifier|is_self_referencing|is_identity|identity_generation|identity_start|identity_increment|   identity_maximum|identity_minimum|identity_cycle|is_generated|generation_expression|is_updatable
        // Example data: ["my_data", "public", "payment", "id", "1", "", "NO", "bigint", "", "", "64", "2", "0", "", "", "", "", "", "", "", "", "", "", "", "", "my_data", "pg_catalog", "int8", "", "", "", "", "1", "NO", "YES", "ALWAYS", "1", "1", "9223372036854775807", "1", "NO", "NEVER", "", "YES"], 
        string query = "SELECT column_name  FROM information_schema.columns WHERE table_catalog = '"
            ~ database ~ "' AND table_name = '" ~ table ~ "';"; // TODO sql injection

        QueryResult queryResult = executeSelectQueryInDB(database, query);
        string[] columns;
        foreach (string[] row; queryResult.rows)
        {
            columns ~= row[0];
        }
        return columns;
    }

    string[] getDatabasesApi(const RequestData requestData, out ResponseData responseData)
    {
        auto databases = getDatabases();
        responseData.statusCode = StatusCode.ok;
        return databases;
    }

}