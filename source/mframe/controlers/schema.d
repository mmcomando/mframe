module mframe.controlers.schema;

import std.conv : to;
import std.stdio: writeln, writefln;

import mframe.controlers.base;
import mframe.db.postgresql; 
import mframe.request;
import mframe.router;  
import mframe.schema;
import mframe.trust;


struct InsertResult
{
    ulong id;
}

struct SchemaApis
{
    Schema schema;
    PostgresConnection postgreDB;

    void register(ref Router router, string path)
    {
        path = path ~ schema.title;
        router.addJSON(path ~ "/schema", &getSchema, TrustLevel.Admin);
        router.addJSON(path ~ "/save", &insert, TrustLevel.Admin);
        router.addJSON(path ~ "/delete", &delete_, TrustLevel.Admin);
        router.addJSON(path ~ "/array", &records, TrustLevel.Admin);
    }

    Schema getSchema(const RequestData requestData, out ResponseData responseData)
    {
        responseData.statusCode = StatusCode.ok;
        return schema;
    }

    void delete_(const RequestData requestData, out ResponseData responseData)
    {
        ulong id = requestData.post.get("id", "0").to!ulong;
        bool ok = postgreDB.deleteById(schema.postgresTableName, id);
        responseData.statusCode = ok ? StatusCode.ok : StatusCode.badRequest;
    }

    InsertResult insert(const RequestData requestData, out ResponseData responseData)
    {
        ulong retId = 0;
        
        string idVal = requestData.post.get("id", "");
        bool makeInsert = idVal == "" || idVal == "0";
        if (makeInsert)
        {
            writeln("INSERT");
            retId = postgreDB.queryInsertBySchema(schema, requestData.post);
        }
        else
        {
            writeln("UPDATE");
            postgreDB.queryUpdateBySchema(schema, requestData.post);
        }

        responseData.statusCode = StatusCode.ok;
        InsertResult result;
        result.id = retId;
        return result;
    }

    auto records(const RequestData requestData, out ResponseData responseData)
    {
        writefln("get records table(%s)", schema.title);
        responseData.statusCode = StatusCode.ok;
        auto retData = postgreDB.getRecordsBySchema(schema);
        writeln(retData);
        return retData;
    }
}

struct SchemaGroupApis
{
    SchemaApis[] schemaApis;


    void register(ref Router router, string path)
    {
        router.addJSON(path ~ "schemas", &getSchemasNames, TrustLevel.Admin);
        path = path ~ "sh/";
        foreach (i, ref api; schemaApis)
        {
            api.register(router, path);
        }
    }

    void initialize(PostgresConnection db, Schema[] schemas)
    {
        schemaApis.length = schemas.length;
        foreach(i, ref schema; schemas)
        {
            schemaApis[i].schema = schema;
            schemaApis[i].postgreDB = db;
        }
    }


    string[] getSchemasNames(const RequestData requestData, out ResponseData responseData)
    {
        string[] apis;
        foreach (ref api; schemaApis)
        {
            apis ~= api.schema.title;
        }
        responseData.statusCode = StatusCode.ok;
        return apis;
    }
}