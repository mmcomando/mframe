module mframe.controlers.session;

import core.stdc.stdio;
import core.sys.posix.unistd;
import std.string : toStringz;
import std.conv : to;
import std.stdio : writeln, writefln;

import mframe.controlers.base;
import mframe.crypto;
import mframe.db.postgresql;
import mframe.router;
import mframe.server;
import mframe.request;
import mframe.schema;
import mframe.trust;
import serializer.json;

import mmutils.log;


struct SessionController
{
    struct UserBasicData
    {
        long id;
        string name;

    }

    struct UserInfo
    {
        long id;
        string name;
        ProcInfo procInfo;
    }

    struct ProcInfo
    {
        long vsize;
        long rss;
        long starttime;
        long handledRequests;
    }

    PostgresConnection connection;
    Cipher cipher;

    void initialize()
    {
        connection.initiaizeEnv();
        cipher.initialize([200, 144, 32, 0, 0, 0, 0, 0, 32, 114, 35, 0, 0, 0, 0, 0],
                [0, 128, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 134, 127, 0, 0]); // TODO load from env
    }

    void register(ref Router router)
    {
        router.addJSON("/api/session/login", &login, TrustLevel.NotAuthenticatedUser);
        router.addJSON("/api/session/get_user_info", &getUserInfo, TrustLevel.AuthenticatedUser);
    }

    void login(const RequestData requestData, out ResponseData responseData)
    {
        string user = requestData.post.get("user", "");
        string password = requestData.post.get("password", "");

        UserBasicData[] ret = connection.getData!UserBasicData(
                "SELECT id, name FROM person WHERE name=$1::text AND password=$2::text LIMIT 1",
                user, password);

        if (ret.length != 1)
        {
            return;
        }

        Cookie cookie;
        cookie.value = cipher.typeToEncryptedBase64JSON(ret[0]);
        cookie.expireDate = 60 * 60 * 60;
        responseData.setCookie["session"] = cookie;

        log(LL(), "User logged id(%llu), name(%s), session(%s)", ret[0].id,
                ret[0].name.toStringz, cookie.value.toStringz);
        responseData.statusCode = StatusCode.ok;

    }

    ProcInfo getProcInfo()
    {
        char[16] pid, comm, state, ppid, pgrp, session, tty_nr;
        char[16] tpgid, flags, minflt, cminflt, majflt, cmajflt;
        char[16] utime, stime, cutime, cstime, priority, nice;
        char[16] O, itrealvalue;

        ProcInfo info;

        FILE* file = fopen("/proc/self/stat", "r");
        int argumentsLoaded = fscanf(file, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %lld %lld %lld",
                pid.ptr, comm.ptr, state.ptr, ppid.ptr, pgrp.ptr, session.ptr,
                tty_nr.ptr, tpgid.ptr, flags.ptr, minflt.ptr, cminflt.ptr,
                majflt.ptr, cmajflt.ptr, utime.ptr, stime.ptr, cutime.ptr,
                cstime.ptr, priority.ptr, nice.ptr, O.ptr, itrealvalue.ptr,
                &info.starttime, &info.vsize, &info.rss);
        fclose(file);
        long pageSize = sysconf(_SC_PAGE_SIZE);
        info.rss *= pageSize;
        info.handledRequests = gHandledRequests;
        assert(argumentsLoaded > 0);


        return info;
    }

    UserInfo getUserInfo(const RequestData requestData, out ResponseData responseData)
    {
        string sessionCookie = requestData.cookie.get("session", "");

        if (sessionCookie.length == 0)
        {
            log(LL(), "No session cookie");
            return UserInfo();
        }
        UserBasicData userData = cipher.encryptedBase64JSONToType!UserBasicData(sessionCookie);

        ubyte[] data = Base64.decode(sessionCookie);
        cipher.decrypt(data); // TODO:  proper verification using secure key should be done, don't know if currently used key is secure

        char[] container = cast(char[]) data;
        
        JSONSerializer.instance.serialize!(Load.yes, true)(userData, container);
        writefln("userInfo(%s)", userData);
        writefln("Logged user (%d)", userData.id != 0);
        UserInfo info;
        info.id = userData.id;
        info.name = userData.name;
        info.procInfo = getProcInfo();
        writeln(info);
        responseData.statusCode = StatusCode.ok;
        return info;

    }
}
