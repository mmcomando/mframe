module mframe.db.postgresql;

import core.stdc.stdio;
import core.stdc.stdlib : getenv, free;
import core.stdc.string : memcpy, strlen, memset;
import core.sys.posix.stdio : fmemopen, open_memstream;
import std.array : split;
import std.stdio : writeln, writefln, File;
import std.string : toStringz, toLower, strip;
import std.conv : to;
import std.format;
import std.bitmanip : swapEndian;
import core.thread;

import std.traits : isNumeric;

import postgresql.libpq_fe;

import mframe.schema;
import mmutils.log;
import serializer.json;

private immutable reconnectionSleepTime = 1.seconds;
private immutable defaultReconnectionTryCount = 1 * 5;

struct PGresultAutoRelese
{
    @disable this(this); // not copyable
    PGresult* result;
    alias result this;

    ~this() nothrow
    {
        PQclear(result);
        result = null;
    }
}

struct PostgresConnection
{
    PGconn* conn = null;
    string server; // localhost, docker container name or server address
    string dbName;
    string dbPort;
    string dbUser;
    string dbPass;
    int reconnectionTryCount = defaultReconnectionTryCount;
    bool createDatabaseOnStartup = true;

    void initiaizeEnv()
    {
        // TODO: move getting parameters to caller
        if (conn != null)
        {
            return;
        }

        string fromEnv(string envName)
        {
            auto cstr = getenv(envName.ptr);
            if (cstr == null)
            {
                return "";
            }
            return cstr[0 .. strlen(cstr)].idup;
        }

        server = fromEnv("POSTGRES_SERVER");
        dbName = fromEnv("POSTGRES_DB");
        dbPort = fromEnv("POSTGRES_PORT");
        dbUser = fromEnv("POSTGRES_USER");
        dbPass = fromEnv("POSTGRES_PASSWORD");

        if(server.length == 0)
        {
            initiaizeEnvFile();// TODO: debug only
        }
        assert(server.length, "Required environment variables are not set");

        // writefln("Initialize database ENV variables: POSTGRES_SERVER(%s), POSTGRES_DB(%s), POSTGRES_PORT(%s), POSTGRES_USER(%s), POSTGRES_PASSWORD(%s)",
        //         server, dbName, dbPort, dbUser, dbPass);
    }

    void initiaizeEnvFile()
    {
        auto file = File(".env");
        auto range = file.byLine();
            writeln("A");
        foreach (line; range)
        {
            if (line.length == 0 || line[0] == '#')
                continue;

            char[][] parts = line.split("=");
            if (parts.length != 2)
                continue;

            string data = parts[1].strip().idup;
            switch(parts[0].strip())
            {
                case "POSTGRES_SERVER": server = data; break;
                case "POSTGRES_DB": dbName = data; break;
                case "POSTGRES_PORT": dbPort = data; break;
                case "POSTGRES_USER": dbUser = data; break;
                case "POSTGRES_PASSWORD": dbPass = data; break;
                default: break;
            }

        }
        assert(server.length && dbName.length && dbPort.length && dbUser.length && dbPass.length, "Required Variables are not set");
    }

    void connect()
    {
        if (conn)
        {
            if (PQstatus(conn) == ConnStatusType.CONNECTION_OK)
            {
                return; // Already connected
            }
            PQfinish(conn);
        }
        // writeln("Database connect");
        foreach (tryCount; 0 .. reconnectionTryCount)
        {
            conn = PQsetdbLogin(server.toStringz, dbPort.toStringz, null, null,
                    dbName.toStringz, dbUser.toStringz, dbPass.toStringz);
            if (PQstatus(conn) == ConnStatusType.CONNECTION_OK)
            {
                break;
            }
            PQfinish(conn);
            conn = null;
            writeln("Database connect tryNum: ", tryCount);
            if (tryCount < reconnectionTryCount - 1)
            {
                Thread.sleep(reconnectionSleepTime);
            }
        }
        if (conn == null && createDatabaseOnStartup == true)
        {
            bool created = createDatabase();
            if (created == false)
            {
                writeln("Can not connect to PostgreSQL, connection is null");
                throw new Exception("Can not connect to PostgreSQL, connection is null");
            }
        }
    }

    void disconnect()
    {
        if (conn == null)
        {
            return;
        }
        PQfinish(conn);
        conn = null;
    }

    bool createDatabase()
    {
        // writeln("createDatabase");
        if (conn)
        {
            PQfinish(conn);
        }
        // Connect to db
        conn = PQsetdbLogin(server.toStringz, dbPort.toStringz, null, null,
                dbName.toStringz, dbUser.toStringz, dbPass.toStringz);
        if (PQstatus(conn) == ConnStatusType.CONNECTION_OK)
        {
            return true;
        }

        // Connect failed, connect again to create DB
        conn = PQsetdbLogin(server.toStringz, dbPort.toStringz, null, null,
                null, dbUser.toStringz, dbPass.toStringz);
        if (PQstatus(conn) != ConnStatusType.CONNECTION_OK)
        {
            writeln("Can not connect to create databse ", PQstatus(conn));
            PQfinish(conn);
            conn = null;
            return false;
        }
        writeln("Connected");
        try
        {
            query("CREATE DATABASE " ~ dbName);
        }
        catch (Exception)
        {
            writeln("Database was not creaated");
        }
        PQfinish(conn);
        conn = null;

        // Connect to db
        conn = PQsetdbLogin(server.ptr, dbPort.ptr, null, null, dbName.ptr,
                dbUser.ptr, dbPass.ptr);
        if (PQstatus(conn) != ConnStatusType.CONNECTION_OK)
        {
            writeln("Can not connect to main database");
            PQfinish(conn);
            conn = null;
            return false;
        }
        return true;
    }

    private alias query_test_instance = query!(int, string, ulong);
    PGresultAutoRelese query(Args...)(string query, Args args)
    {
        auto perfCheck =log_perf(LL(LogType.Info, LogPriority.RarelyUseful), "SQL", "SQL query(%.*s)", query.length, query.ptr);
        connect();

        int nParams = args.length;
        const(char)*[args.length] paramValues;
        char[256][args.length] stringValues;
        size_t[args.length] intValues;
        int[args.length] paramLengths;
        int[args.length] paramFormats;
        int resultFormat = 0;

        static foreach (i, arg; args)
        {
            static if (is(typeof(arg) == string))
            {
                memcpy(stringValues[i].ptr, arg.ptr, arg.length);
                stringValues[i][arg.length] = 0;
                paramValues[i] = cast(const char*) stringValues[i].ptr;
                paramLengths[i] = cast(int) arg.length;
                paramFormats[i] = 0; //string

            }
            else
            {
                intValues[i] = arg;

                version (LittleEndian)
                {
                    intValues[i] = intValues[i].swapEndian;
                }

                paramValues[i] = cast(const char*)&intValues[i];
                paramLengths[i] = intValues[i].sizeof;
                paramFormats[i] = 1; // binary

            }
        }

        query = query ~ '\0'; // TODO mega hack
        query = query[0 .. $ - 1];
        // fprintf(stderr, "Query(%s)\n", query.ptr);
        PGresult* resultRaw = PQexecParams(conn, query.ptr, nParams, null,
                paramValues.ptr, paramLengths.ptr, paramFormats.ptr, resultFormat);

        ExecStatusType status = PQresultStatus(resultRaw);

        bool failed = status != ExecStatusType.PGRES_COMMAND_OK
            && status != ExecStatusType.PGRES_TUPLES_OK;

        if (failed)
        {
            // fprintf(stderr, "Query(%s) failed(%d): Error(%s)\n", query.ptr,
            // cast(int) PQresultStatus(result), PQresultErrorMessage(result));
            char* ptr = PQresultErrorMessage(resultRaw);
            throw new Exception("Query failed. Query(%s), Status(%d), Error Message(%s)".format(query,
                    status, ptr[0 .. strlen(ptr)]));

        }
        PGresultAutoRelese result = PGresultAutoRelese(resultRaw);
        return result;
    }

    private alias queryBool_test_instance = queryBool!(int, string, ulong);
    bool queryBool(Args...)(string queryStr, Args args)
    {
        auto result = query(queryStr, args);
        char* value = PQgetvalue(result, 0, 0);
        assert(value);

        if (value[0] == 't')
        {
            return true;
        }
        if (value[0] == 'f')
        {
            return false;
        }

        throw new Exception("Result is not true/false");
    }

    bool queryTableExists(Args...)(string tableName)
    {
        return queryBool("
SELECT EXISTS (
    SELECT *
    FROM   information_schema.tables 
    WHERE  table_schema = 'public'
    AND    table_name = LOWER('%s')
);   
".format(tableName));
    }

    bool queryColumnExists(Args...)(string tableName, string columnName)
    {
        return queryBool("
SELECT EXISTS (
    SELECT column_name 
    FROM information_schema.columns 
    WHERE table_name=LOWER('%s') and column_name=LOWER('%s')
);
    ".format(tableName, columnName));
    }

    void resultPrint(PGresult* result)
    {
        char[2] separator = ['|', '\0'];

        PQprintOpt opt;
        opt.header = true;
        opt.align_ = true;
        opt.standard = false;
        opt.html3 = false;
        opt.expanded = false;
        opt.pager = false;
        opt.fieldSep = separator.ptr;
        opt.tableOpt = null;
        opt.caption = null;
        opt.fieldName = null;

        PQprint(stdout, result, &opt);
    }

    string resultPrintToString(PGresult* result)
    {
        char[2] separator = ['|', '\0'];

        PQprintOpt opt;
        opt.header = true;
        opt.align_ = true;
        opt.standard = false;
        opt.html3 = false;
        opt.expanded = false;
        opt.pager = false;
        opt.fieldSep = separator.ptr;
        opt.tableOpt = null;
        opt.caption = null;
        opt.fieldName = null;

        char* buffer;
        size_t len;
        FILE* stream = open_memstream(&buffer, &len);
        // scope (exit)
        //     fclose(stream);// TODO
        scope (exit)
            free(buffer);

        PQprint(stream, result, &opt);
        fflush(stream);
        string output = buffer[0 .. len].idup;
        return output;
    }

    auto queryPrint(Args...)(string queryStr, Args args)
    {
        auto result = query(queryStr, args);
        resultPrint(result);

        return result;
    }

    ulong PQgetinteger(PGresult* result, int row, int col)
    {
        struct Data
        {
            union
            {
                ulong int8;
                int int4;
                ushort int2;
            }
        }

        Data data;
        char* dataVal = PQgetvalue(result, row, col);
        int length = PQgetlength(result, row, col);

        assert(length == 2 || length == 4 || length == 8);

        memcpy(&data, dataVal, length);

        ulong retVal = 0;

        auto swap(T)(T el)
        {
            version (LittleEndian)
            {
                el = el.swapEndian;
                return el;
            }
        }

        if (length == 2)
        {
            retVal = swap(data.int2);
        }
        if (length == 4)
        {
            retVal = swap(data.int4);
        }
        else
        {
            retVal = swap(data.int8);

        }

        return retVal;
    }

    private string[string] structToDataMap(T)(ref T st)
    {
        string[string] data;
        foreach (i, ref elem; st.tupleof)
        {
            enum string name = __traits(identifier, T.tupleof[i]);
            data[name] = elem.to!string;
        }
        return data;
    }

    ulong queryInsert(T)(ref const Schema schema, ref T data)
    {
        return queryInsertBySchema(schema, structToDataMap!T(data));
    }

    ulong queryUpdate(T)(ref const Schema schema, ref T data)
    {
        return queryUpdateBySchema(schema, structToDataMap!T(data));
    }

    ulong queryInsertBySchema(ref const Schema schema, const(string[string]) data)
    {
        assert(data.length > 0);
        string queryStr = "INSERT INTO ";
        queryStr ~= schema.postgresTableName;
        queryStr ~= " (";
        foreach (ref property; schema.properties)
        {
            if (property.title == "id")
            {
                continue; // Ignore id in Insert query
            }
            queryStr ~= property.title;
            queryStr ~= ", ";
        }

        queryStr = queryStr[0 .. $ - 2]; // remove last ','

        queryStr ~= " ) VALUES(";
            writeln(data);
        foreach (ref property; schema.properties)
        {
            if (property.title == "id")
            {
                continue; // Ignore id in Insert query
            }
            string defaultValue = "";

            if (property.type == "integer")
            {
                defaultValue = "0";
            }
            writeln(property.title);
            string val = data.get(property.title, defaultValue);
            foreach (c; val)
                assert(c != '\'' && c != '\0');

            queryStr ~= "'";
            queryStr ~= val; // TODO very bad
            queryStr ~= "', ";
        }

        queryStr = queryStr[0 .. $ - 2]; // remove last ','

        queryStr ~= ") RETURNING id;";

        auto result = queryPrint(queryStr);
        char* idStr = PQgetvalue(result, 0, 0);
        ulong id = idStr[0 .. PQgetlength(result, 0, 0)].to!ulong;
        return id;
    }

    auto queryUpdateBySchema(const ref Schema schema, const(string[string]) data)
    {
        string queryStr = "UPDATE ";
        queryStr ~= schema.postgresTableName;
        queryStr ~= " SET ";

        foreach (ref property; schema.properties)
        {
            if (property.title == "id")
            {
                continue; // Don't update id
            }

            string val = data.get(property.title, "");
            foreach (c; val)
                assert(c != '\'' && c != '\0');

            queryStr ~= property.title;
            queryStr ~= " = ";
            queryStr ~= "'";
            queryStr ~= val; // TODO very bad 
            queryStr ~= "', ";
        }

        queryStr = queryStr[0 .. $ - 2]; // remove last ','

        queryStr ~= "\nWHERE id = ";
        queryStr ~= "'";
        queryStr ~= data["id"]; // TODO very bad 
        queryStr ~= "';";

        auto ok = queryPrint(queryStr);
        return ok;
    }

    string[] getUpdateDatabaseModelQueries(Schema schema)
    {
        string[] updateQueries;

        bool tableExists = queryTableExists(schema.postgresTableName);
        if (tableExists == false)
        {
            updateQueries ~= "CREATE TABLE %s (id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY);".format(
                    schema.postgresTableName);
        }
        // bool idColumnExists = queryColumnExists(conn, schema.postgresTableName, "id");
        // if (idColumnExists == false)
        // {
        //     updateQueries ~= "ALTER TABLE %s ADD COLUMN id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY;".format(
        //             schema.postgresTableName);
        // }

        foreach (keyVal; schema.properties.byKeyValue)
        {
            string type = "VARCHAR";
            switch (keyVal.value.type)
            {
            case "integer":
                type = "BIGINT";
                break;
            default:
                break;
            }

            bool columnExists = queryColumnExists(schema.postgresTableName, keyVal.key)
                || keyVal.key == "id";
            if (columnExists == false)
            {
                updateQueries ~= "ALTER TABLE %s ADD COLUMN %s %s;".format(schema.postgresTableName,
                        keyVal.key, type);
            }
            updateQueries ~= "ALTER TABLE %1$s ALTER COLUMN %2$s TYPE %3$s USING %2$s::%3$s;".format(
                    schema.postgresTableName, keyVal.key, type);
        }

        return updateQueries;
    }

    bool executeQuery(string queryStr)
    {
        try
        {
            writefln("Execute Query(%s)", queryStr);
            queryPrint(queryStr);
            return true;
        }
        catch (Exception e)
        {
            writeln(e);
        }
        return false;
    }

    bool deleteById(string tableName, ulong id)
    {
        try
        {
            string queryStr = "DELETE FROM ";
            queryStr ~= tableName;
            queryStr ~= " WHERE id=";
            queryStr ~= id.to!string;
            queryStr ~= ";";
            auto ok = queryPrint(queryStr);
        }
        catch (Exception e)
        {
            writeln(e);
            return false;
        }
        return true;
    }

    T getById(T)(string tableName, ulong id)
    {
        T[] array = getRecordsHelper!(T)(tableName, id, true);
        if (array.length == 0)
        {
            throw new Exception("No record with this id");
        }
        return array[0];
    }

    T[] getRecords(T)(string tableName)
    {
        T[] array = getRecordsHelper!(T)(tableName, 0, false);
        return array;
    }

    string[string][] getRecordsBySchema(Schema schema)
    {
        string[string][] array = getRecordsHelperBySchema(schema, 0, false);
        return array;
    }

    private string[string][] getRecordsHelperBySchema(Schema schema, ulong id, bool addWhere)
    {
        string queryStr = "SELECT ";
        foreach (ref key_val; schema.properties.byKeyValue())
        {
            queryStr ~= key_val.key;
            queryStr ~= ", ";
        }
        queryStr = queryStr[0 .. $ - 2]; // remove last ','

        queryStr ~= " FROM ";
        queryStr ~= schema.postgresTableName;
        if (addWhere)
        {
            queryStr ~= " WHERE id=$1";
        }

        PGresultAutoRelese result = addWhere ? queryPrint(queryStr, id) : queryPrint(queryStr);

        int rowsNum = PQntuples(result);
        if (rowsNum == 0)
        {
            return [];
        }

        string[string][] array;
        foreach (rowNum; 0 .. rowsNum)
        {
            string[string] data;
            int i = 0;
            foreach (ref key_val; schema.properties.byKeyValue())
            {
                char* val = PQgetvalue(result, rowNum, i);
                int len = PQgetlength(result, rowNum, i);
                data[key_val.key] = val[0 .. len].idup;
                i++;
            }
            array ~= data;
        }

        return array;
    }

    bool setStructValue(ST, VAL)(ref ST st, const(char[]) memberName, VAL val)
    {
        foreach (i, ref elem; st.tupleof)
        {
            enum string name = __traits(identifier, ST.tupleof[i]).toLower;
            alias Type = typeof(elem);
            if(memberName.toLower != name)
            {
                continue;
            }
            static if (is(Type == string) && is(VAL == char[]))
            {
                elem = val.idup;
                return true;
            }
            else static if (isNumeric!Type && is(VAL == char[]))
            {
                elem = val.to!Type;
                return true;
            }
        }
        return false;
    }

    T[] getData(T, Args...)(string query, Args args)
    {

        PGresultAutoRelese result = queryPrint(query, args);

        int rowsNum = PQntuples(result);
        int colsNum = PQnfields(result);
        if (colsNum <= 0)
        {
            log(LL(LogType.Error), "colsNum(%d), QQ(%s)", query.toStringz, colsNum);
            return [];
        }
        if (rowsNum == 0)
        {
            log(LL(), "rowsNum(%d), QQ(%s)", query.toStringz, rowsNum);
            return [];
        }

        T[] array;
        array.length = rowsNum;

        foreach (rowNum; 0 .. rowsNum)
        {
            foreach (colNum; 0 .. colsNum)
            {
                int colFormat = PQfformat(result, colNum);
                char* colNamePtr = PQfname(result, colNum);
                char[] colName = colNamePtr[0..strlen(colNamePtr)];

                T* data = &array[rowNum];

                if(colFormat == 0)
                {
                    // Text
                    char* valPtr = PQgetvalue(result, rowNum, colNum);
                    int len = PQgetlength(result, rowNum, colNum);
                    char[] val =  valPtr[0..len];
                    setStructValue(*data, colName, val);
                }
                else
                {
                    // Binary
                    assert(0);
                }
            }

        }
        return array;
    }

        private T[] getRecordsHelper(T)(string tableName, ulong id, bool addWhere)
        {
            string queryStr = "SELECT ";
            T helper;
            foreach (i, ref elem; helper.tupleof)
            {
                enum string name = __traits(identifier, T.tupleof[i]);
                queryStr ~= name;
                static if (i < cast(int) T.tupleof.length - 1)
                {
                    queryStr ~= ", ";
                }
            }
            queryStr ~= " FROM ";
            queryStr ~= tableName;
            if (addWhere)
            {
                queryStr ~= " WHERE id=$1";
            }

            PGresultAutoRelese result = addWhere ? queryPrint(queryStr, id) : queryPrint(queryStr);

            int rowsNum = PQntuples(result);
            if (rowsNum == 0)
            {
                return [];
            }

            T[] array;
            foreach (rowNum; 0 .. rowsNum)
            {
                T data;
                foreach (i, ref elem; data.tupleof)
                {
                    alias Type = typeof(elem);
                    static if (is(Type == string))
                    {
                        char* val = PQgetvalue(result, rowNum, i);
                        int len = PQgetlength(result, rowNum, i);
                        elem = val[0 .. len].idup;
                    }
                    else static if (isNumeric!Type)
                    {
                        char* val = PQgetvalue(result, rowNum, i);
                        int len = PQgetlength(result, rowNum, i);
                        elem = val[0 .. len].to!Type;
                    }
                    else
                    {
                        static assert(0);
                    }
                }
                array ~= data;
            }

            return array;
        }

        string[] getResultColumnNames(PGresult* result)
        {

            int colsNum = PQnfields(result);

            if (colsNum == 0)
            {
                return null;
            }

            string[] columnNames;
            columnNames.length = colsNum;

            foreach (colNum; 0 .. colsNum)
            {
                char* name = PQfname(result, colNum);
                size_t len = strlen(name);
                columnNames[colNum] = name[0 .. len].idup;
            }

            return columnNames;
        }

        string[][] getResultAsStrings(PGresult* result)
        {

            int rowsNum = PQntuples(result);
            int colsNum = PQnfields(result);

            if (rowsNum == 0)
            {
                return null;
            }

            string[][] returnData;
            returnData.length = rowsNum;

            foreach (rowNum; 0 .. rowsNum)
            {
                returnData[rowNum].length = colsNum;
                foreach (colNum; 0 .. colsNum)
                {
                    char* val = PQgetvalue(result, rowNum, colNum);
                    int len = PQgetlength(result, rowNum, colNum);
                    returnData[rowNum][colNum] = val[0 .. len].idup;
                }
            }

            return returnData;
        }

    }
