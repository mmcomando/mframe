module mframe.router;

import std.traits;
import std.stdio;

import mframe.request;
import mframe.trust;
import serializer.json;


struct ControlerData
{
    ControlerFunc func;
    TrustLevel requiredTrustLevel;// Trust which user has to have to execute controller
}

/// Optional data stored bu user of framework


struct Router
{
    ControlerData[string] controlers;
    RequestDataInitFunc requestDataInitFunc;
    void* userData;

    void addJSON(FUNC)(string path, FUNC func, TrustLevel requiredTrustLevel)
    {

        string toJSON(RequestData data, out ResponseData responseData)
        {
            alias RetType = ReturnType!FUNC;
            static if (is(RetType == void))
            {
                func(data, responseData);
                return "{}";
            }
            else
            {
                auto retData = func(data, responseData);
                char[] container;
                JSONSerializer.instance.serialize!(Load.no, true)(retData, container);
                return container.idup;

            }

        }
        ControlerData data = ControlerData(&toJSON, requiredTrustLevel);
        controlers[path] = data;
    }

    void setRequestDataInitFunc(RequestDataInitFunc requestDataInitFunc)
    {
        this.requestDataInitFunc = requestDataInitFunc;
    }

    string noController(RequestData data, out ResponseData responseData)
    {
        writeln("No controller");
        return "No controller: " ~ data.page;
    }

    ControlerData getControler(string path)
    {
        return controlers.get(path, ControlerData(&noController, TrustLevel.NoTrust));
    }

    ControlerFunc getControlerFunc(string path)
    {
        return getControler(path).func;
    }
}
