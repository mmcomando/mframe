module mframe.request;

import std.conv : to;

import mframe.multipart;
import mframe.schema;
import mframe.trust;

struct RequestData
{
    string page;
    string[string] post;
    string[string] cookie;
    string[string] get;
    MultipartPart[] multipart;
    TrustLevel trustLevel = TrustLevel.NoTrust;
    long userId;
    void* userData;
}

struct Cookie
{
    string value;
    int expireDate = int.max;
    bool secure = false;
    bool httpOnly = true;
}

enum StatusCode : int
{
    ok = 200,
    created = 201,
    accepted = 202,

    redirectFound = 302,
    redirectOther = 303,
    redirectTemporary = 307,
    redirectNotModified = 304,
    redirectMultipleChoice = 300,

    badRequest = 400,
    unauthorized = 401,
    paymentRequired = 402,
    forbidden = 403,
    notFound = 404,
    methodNotAllowed = 405,
}

struct ResponseData
{
    Cookie[string] setCookie;
    string redirectPath;
    StatusCode statusCode = StatusCode.badRequest;
    int customCode;

    void setRedirectHeader(string path, StatusCode redirectStatusCode = StatusCode.redirectFound)
    {
        assert(redirectStatusCode >= 300 && redirectStatusCode < 400);
        redirectPath = path;
        statusCode = redirectStatusCode;
    }
}

alias RequestDataInitFunc = void delegate(ref RequestData requestData);
alias ControlerFunc = string delegate(RequestData requestData, out ResponseData responseData);

auto getFromRequest(T)(const RequestData input, bool fromPost)
{
    const string[string]* map = fromPost ? &input.post : &input.get;

    T data;

    foreach (i, ref elem; data.tupleof)
    {
        alias Type = typeof(elem);
        enum string name = __traits(identifier, T.tupleof[i]);

        string valueStr = map.get(name, "");
        try
        {
            elem = valueStr.to!Type;
        }
        catch (Exception e)
        {
        }
    }
    return data;
}
