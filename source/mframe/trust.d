module mframe.trust;


enum TrustLevel : int
{
    NoTrust, /// Default, don't trust, maybe reduce content
    NotAuthenticatedUser, /// Not logged user, random viewer or crawler
    AuthenticatedUser, /// User who authenticated in the website
    TrustedUser, /// User which we trust a bit
    Admin, /// Application administrator/maintainer
    ApplicationCreator, /// Application creator, full access
}

