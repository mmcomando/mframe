# Script used to build mframe docker conainers
# Ex. python3 deploy_mframe.py /web/mframe https://gitlab.com/mmcomando/mframe.git
import argparse
import os
import subprocess

# Script arguments
parser = argparse.ArgumentParser(description='Build mframe docker conainers.')
parser.add_argument("deploy_path", help="Directory in which project files will be saved")
parser.add_argument("git_repo", help="Git repo clone URL")
args = parser.parse_args()

# Parse aguments
deploy_path_split = os.path.split(args.deploy_path)

proj_path = args.deploy_path + '/'
proj_git_repo = args.git_repo

# Script

if os.path.isdir(proj_path) == False:
    print('Project does not exist, path(%s)' % proj_path)
    subprocess.check_call(['git', 'clone', proj_git_repo, proj_path])

print('Git pull')
subprocess.check_call(['git', '-C', proj_path, 'pull'])

print('Build and push dlang image')
cwd = '%sdocker/dlang' % proj_path
subprocess.check_call(['docker-compose', 'build'], cwd=cwd)
subprocess.check_call(['docker-compose', 'push'], cwd=cwd)

print('Build and push mframe image')
cwd = proj_path
subprocess.check_call(['docker-compose', 'build'], cwd=cwd)
subprocess.check_call(['docker-compose', 'push'], cwd=cwd)
