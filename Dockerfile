FROM mmcomando/dlang

# mframe dependencies
RUN apt-get update && apt-get install -y \
    git \
    libfcgi-dev \
    libpq-dev \
    libtomcrypt-dev

# Get latest versions of D dependencies
RUN mkdir /dependencies
RUN git clone https://github.com/NCrashed/libtomcrypt /dependencies/libtomcrypt
RUN dub add-path /dependencies

# mframe will be dependency for other projects so place itself there as well
RUN mkdir /dependencies/mframe
COPY . /dependencies/mframe

# Build
RUN dub --root=/dependencies/mframe build
